# UrlcheckerCalife

UrlcheckerCalife - A ruby gem for checking links (public or private).  Multi-threaded, with text/based output, support for SSL.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'urlchecker_calife'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install urlchecker_calife

## Usage

To perform a check for urls defined in configuration file.

From a command console type:

$ urlchecker

Exits with status 0 if all links are available, , greater than 0 if errors occur.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake false` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/calife/urchecker_calife .


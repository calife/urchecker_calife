# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'urlchecker_calife/version'

Gem::Specification.new do |spec|
  spec.name          = "urlchecker_calife"
  spec.version       = UrlcheckerCalife::VERSION
  spec.authors       = ["calife"]
  spec.email         = ["califerno@gmail.com"]

  spec.summary       = %q{Url Checker application.}
  spec.description   = %q{Url Checker application with http/https support.}
  spec.homepage      = "https://bitbucket.org/calife/urchecker_calife"  

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  # if spec.respond_to?(:metadata)
  #   spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  # else
  #   raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  # end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }  
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"

  # spec.add_runtime_dependency  "net/http" ,"~>2.7.0.1"
  # spec.add_runtime_dependency  "socket" , ""
  # spec.add_runtime_dependency  "openssl" , ""
  # spec.add_runtime_dependency  "logger" , ""
  # spec.add_runtime_dependency  "optparse" , ""
  
end

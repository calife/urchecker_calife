require "urlchecker_calife/version"

require 'net/http'
require 'socket'
require 'openssl'
require 'logger'
require 'optparse'
require 'pp'

module Logging

  @logger=nil

  def logger(level=Logger::DEBUG)
    @logger ||= Logger.new(STDOUT)
    @logger.level = level
    @logger
  end
  
end


module UrlcheckerCalife
  
  class Checker
    
    include Logging # Mix in the ability to log stuff ...
    
    attr_reader :return_code
    attr_reader :results
    
    # Create a new instance of Checker
    #
    # @param params [Hash] A hash containing the :target value, which can represent either a file path or a URL.
    # And an optional :options value, which contains a hash with a list of possible optional paramters.
    def initialize(params)
      
      @options = params

      @return_code = 0

      @debug =  @options.key?(:debug) || false
      
      @options[:max_threads] ||= 100

      @results||={}

      @urls||=[]

      @urls.push( # from file
        File.readlines(@options[:filename])
        .delete_if { |x| x =~ /^[ \t]*$/ } # empty line
          .delete_if { |x| x =~ /^[ \t]*#.*$/ } # comment
            .map { |x| x.gsub!(/\t|\n|\s+/, "") } ) if @options[:filename] 

      @urls << @options[:data_array] if @options[:data_array]  # from data_array
      
      @urls.flatten!
      
    end

    def do_it_well()

      starttime=Time.now
      
      threads=[]
      
      @urls.each { |url|

        wait_to_spawn_thread

        threads << Thread.new(url) {

          Signal.trap("INT") {
            puts "Manually interrupted........."
            exit 1
          }    

          Thread.current[:url]=url

          begin

            if url.include? "https"
              
              host=url.gsub("https://","")

              if host =~ /(.*)(:)(.*)/i
                host=$1          
                port=$3
              else
                port=443
              end              

              HttpxClient.new.connect_https(host,port)
              
            else
              
              host=url.gsub("http://","")
              
              if host =~ /(.*)(:)(.*)/i
                host=$1
                port=$3
              else
                port=80
              end        
              
              HttpxClient.new.connect_http(host,port)
              
            end
            
            Thread.current[:status]=true
            
          rescue Exception
            Thread.current[:status]=false
            @return_code+=1
          end
          
        }

      }

      threads.each { |t| t.join }
      
      @results= threads.collect { |t|
        { url: t[:url], status: t[:status]}
      }

      logger.info "Time elapsed #{Time.now - starttime} seconds"
      
    end

    def print_result

      $num_chars=90
      
      count=1

      logger.info ( sprintf "%#{$num_chars}s" , "#{ '*' * $num_chars }" )
      logger.info ( sprintf "%s" , "URL Queue [#{@results.size} elements]" )
      @results.each { |t|
        logger.info ( sprintf "%3d - Url: %-40s  - Status: %12s" , count, t[:url]  , t[:status] )
        count+=1
      }
      logger.info ( sprintf "%#{$num_chars}s" , "#{ '*' * $num_chars }" )
      
    end

    private
    
    # Checks the current :max_threads setting and blocks until the number of threads is below that number.
    def wait_to_spawn_thread
      # Never spawn more than the specified maximum number of threads.
      until Thread.list.select {|thread| thread.status == "run"}.count <
            (1 + @options[:max_threads]) do
        sleep 5 # Wait 5 seconds before trying again.
      end
    end
    

  end
  
  class HttpxClient

    include Logging # Mix in the ability to log stuff ...

    private
    def print_certificate(socket)

      cert=OpenSSL::X509::Certificate.new(socket.peer_cert)

      certprops = OpenSSL::X509::Name.new(cert.issuer).to_a
      issuer = certprops.select { |name, data, type| name == "O" }.first[1]
      results = { 
        :valid_on => cert.not_before,
        :valid_until => cert.not_after,
        :issuer => issuer,
        :valid => (socket.verify_result == 0)
      }

      logger.debug "#{results}"
      
    end

    public
    
    def connect_http(host,port=80)

      begin
        
        h = Net::HTTP.new(host, port)
        h.read_timeout = 2000 # 1s. timeout
        
        h.get('/', nil )    

      rescue Exception => e
        logger.error "connect_http #{Thread.current} #{host} #{port} failed to connect."
        raise e
      end
      
      logger.debug "connect_http #{Thread.current} #{host} #{port} connected."
      return true
      
    end
    
    
    def connect_https(host,port=443)

      begin
        
        sock = TCPSocket.new(host, port)
        ctx = OpenSSL::SSL::SSLContext.new
        ctx.set_params(verify_mode: OpenSSL::SSL::VERIFY_PEER)

        @socket = OpenSSL::SSL::SSLSocket.new(sock, ctx).tap do |socket|

          socket.sync_close = true
          socket.connect

          print_certificate socket if logger.level==Logger::DEBUG
          
          socket.close
          
        end

      rescue Exception => e
        logger.error "connect_http #{Thread.current} #{host} #{port} failed to connect."
        raise e
      end

      logger.debug "connect_https #{Thread.current} #{host} #{port} connected."
      return true
      
    end
    
  end # HttpxClient


  class Cli

    def self.to_array(arr=[])

      options={}
      options[:data_array]=arr
      
      checker= Checker.new(options)
      checker.do_it_well
      
      return checker.results
    end
    
    def self.print

      options = {}
      
      # Default values
      options[:filename]=ENV["HOME"]+"/.urlchecker_calife.urls"

      parser=OptionParser.new do |opts|
        opts.banner = "Usage:#{ __FILE__} [options]"

        opts.on("-d", "--[no-]debug", "Run debug") do |d|
          options[:debug] = d
        end
        
        opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
          options[:verbose] = v
        end

        opts.on('-fINFILE', '--filename INFILE',  "Filename defaults to #{options[:filename]}") do |filename|
  	      options[:filename] = filename
        end

        opts.on('-h', '--help', 'Help') do
	      puts opts
	      return 0
        end
        
        opts.on("--version", "print Version") do |v|
	      puts "Who: UrlcheckerCalife - Age: #{UrlcheckerCalife::VERSION}"
          return 0
        end
        
      end

      parser.parse!

      # Check filename
      unless File.exists? options[:filename]
        puts "[ERROR] Filename #{options[:filename]} does not exists"
        puts "[ERROR] You can find an updated revision HERE"
        return 1
      end
      # End check

      puts "[INFO] Reading urls from file #{options[:filename]}"
      sleep 3

      checker= Checker.new(options)
      
      checker . do_it_well
      
      checker . print_result

      return checker.return_code
      
    end
    
  end # Cli
  
end # Module UrlcheckerCalife
